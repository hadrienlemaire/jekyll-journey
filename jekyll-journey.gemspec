Gem::Specification.new do |s|
  s.name        = 'jekyll-journey'
  s.version     = '0.1.0'
  s.summary     = 'A plugin to display journeys using GPX files.'
  s.description = ''
  s.authors     = ['Hadrien LEMAIRE']
  s.email       = ''
  s.files       = [
    'lib/jekyll-journey.rb',
  ]
  s.license     = 'LGPL-3.0-only'

  s.add_dependency 'jekyll', '~> 4.2', '>= 4.2'
  s.add_dependency 'nokogiri', '~> 1.12', '>= 1.12.4'
  s.add_dependency 'exiftool', '~> 1.2', '>= 1.2.4'
end
