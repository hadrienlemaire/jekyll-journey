require 'nokogiri'
require 'json'
require 'net/http'
require 'uri'
require 'exiftool'

class JourneyCommand < Jekyll::Command
  class << self
    def init_with_program(prog)
      prog.command(:journey) do |c|
        c.syntax 'journey NAME [options]'
        c.description 'Convert .gpx files into .geojson and display them on a single page.'
        c.option 'traces', '-t', '--traces DIR', 'Convert gpx files into geojson and include them.'
        c.option 'images', '-i', '--images DIR', 'Include images.'
        c.option 'output', '-o', '--output DIR', 'Relative output path of the content (DIR/NAME/{traces,images}/*.{geojson, png}).'

        c.action do |args, options|
          unless args[0] then raise 'A name is needed to identificate the journey.' end
          if args[0].start_with?('_') then raise 'The name of the journey cannot start with an underscore.' end

          dest = configuration_from_options({})['source']
          dest_rel = options['output'] ? File.join(options['output'], args[0]) : File.join('journeys', args[0])
          dest_data = File.join(dest, '_data', 'journeys', args[0])
          dest_traces = File.join(dest, dest_rel, 'traces')
          dest_images = File.join(dest, dest_rel, 'images')
          dest_traces_rel = File.join(dest_rel, 'traces')
          dest_images_rel = File.join(dest_rel, 'images')
          mkdir_p(dest_data)
          mkdir_p(dest_traces)
          mkdir_p(dest_images)

          if options['traces']
            path = options['traces']
            unless path then raise 'A path toward a directoy containing .gpx files is needeed.' end
            unless Dir.exist?(path) then raise "The given path (#{path}) doesn't exist." end
            puts "Converting traces from #{path} to #{dest_traces}"
            data = {list: [], data: {}}

            Dir.each_child(path).each do |input_path|
              input_path = File.join(path, input_path)
              next unless File.file?(input_path) and File.extname(input_path) == '.gpx'
              basename = File.basename(input_path, '.gpx')
              print "\rWorking on #{basename}"
              # puts "Working on #{basename}"

              content = convert(File.read(input_path), basename)
              File.write(File.join(dest_traces, basename + '.geojson'), content)

              json = JSON.parse(content, {symbolize_names: true})
              # puts json[:properties]
              data[:list].push(basename)
              data[:data][basename] = {
                path: File.join(dest_traces_rel, basename + '.geojson'),
                city_start: json[:properties][:city_start],
                city_end: json[:properties][:city_end],
                distance: json[:properties][:distance],
                pos_elev_gain: json[:properties][:pos_elev_gain],
                neg_elev_gain: json[:properties][:neg_elev_gain],
                center: get_center(json[:bbox]),
                bbox: json[:bbox]
              }
            end

            bbox_all = data[:data].values.map{|e| e[:bbox]}.transpose
            bbox_all = [ bbox_all[0].min, bbox_all[1].min, bbox_all[2].min, bbox_all[3].max, bbox_all[4].max, bbox_all[5].max ]
            data[:data][:all] = {
              distance: data[:data].values.map{|e| e[:distance]}.sum,
              pos_elev_gain: data[:data].values.map{|e| e[:pos_elev_gain]}.sum,
              neg_elev_gain: data[:data].values.map{|e| e[:neg_elev_gain]}.sum,
              center: get_center(bbox_all),
              bbox: bbox_all,
            }

            data[:list].sort!
            File.write(File.join(dest_data, 'traces.json'), data.to_json)
          end

          if options['images']
            path = options['images']
            unless path then raise 'A path toward a directoy containing images is needeed.' end
            unless Dir.exist?(path) then raise "The given path (#{path}) doesn't exist." end
            puts "Moving images from #{path} to #{dest_images}"
            data = {list: [], data: {}}

            FileUtils.copy_entry(path, dest_images)
            
            exifs = Exiftool.new(Dir[File.join(dest_images, '*')])
            exifs.files_with_results.each do |input_path|
              exif = exifs.result_for(input_path)
              name = exif[:file_name]

              if exif[:gps_altitude]
                alt = exif[:gps_altitude].split(' ')
                alt = alt[2].start_with?('A') ? alt[0].to_f : -1 * alt[0].to_f
              end
              
              data[:list].push(name)
              data[:data][name] = {
                path: File.join(dest_images_rel, name),
                date: exif[:date_time_original],
                lat: exif[:gps_latitude],
                lon: exif[:gps_longitude],
                elev: alt,
              }
            end

            data[:list].sort_by! {|slug| data[:data][slug][:date]}
            date_array = data[:list].map {|slug| data[:data][slug][:date].split(" ")[0]}.uniq!
            data[:data].each_value {|elem| elem[:step] = date_array.find_index(elem[:date].split(" ")[0])}
            data[:list].each {|slug| data[:data][slug].reject! {|key, value| key == :date}}

            puts 'Cleaning files'
            `for i in #{dest_images}; do
              convert "$i" -auto-orient "$i";
              exiftool -all:all= -tagsFromFile @ -exif:Orientation -q -q -overwrite_original "$i";
            done`

            File.write(File.join(dest_data, 'images.json'), data.to_json)
          end
        end
      end
    end

    def convert(content, name)
      xml = Nokogiri::XML(content)

      nb_segments = xml.css('trkseg').length
      coordinates = Array::new(nb_segments)
      distances = Array::new(nb_segments) { 0 }
      pos_elev_gains = Array::new(nb_segments) { 0 }
      neg_elev_gains = Array::new(nb_segments) { 0 }

      xml.css('trkseg').each_with_index do |segment, i|
        nb_points = segment.css('trkpt').length
        coordinates[i] = Array::new(nb_points) { Array::new(3) }
        j_last_elev = 0

        segment.css('trkpt').each_with_index do |point, j|
          coordinates[i][j][0] = point.attr('lon').to_f
          coordinates[i][j][1] = point.attr('lat').to_f
          coordinates[i][j][2] = point.at_css('ele').content.to_f

          unless (nb_points < 1 or j < 1)
            distances[i] += calculate_distance(
              coordinates[i][j-1][0], coordinates[i][j-1][1],
              coordinates[i][j][0], coordinates[i][j][1])
            
            dz = coordinates[i][j][2] - coordinates[i][j_last_elev][2]
            dx = calculate_distance(
              coordinates[i][j_last_elev][0], coordinates[i][j_last_elev][1],
              coordinates[i][j][0], coordinates[i][j][1])
            if dz.abs > 5 and dx > 20
              j_last_elev = j
              dz.negative? ? neg_elev_gains[i] += dz : pos_elev_gains[i] += dz
            end
          end
        end
      end

      lons, lats, elevs = coordinates.flatten(1).transpose()
      bbox = [
        lons.min, lats.min, elevs.min,
        lons.max, lats.max, elevs.max
      ]

      city_start = get_city_name(coordinates[0][0][0], coordinates[0][0][1])
      city_end = get_city_name(coordinates[-1][-1][0], coordinates[-1][-1][1])

      {
        type: 'Feature',
        properties: {
          id: name,
          distance: distances.sum,
          pos_elev_gain: pos_elev_gains.sum,
          neg_elev_gain: neg_elev_gains.sum,
          city_start: city_start,
          city_end: city_end,
        },
        bbox: bbox,
        geometry: {
          type: 'MultiLineString',
          coordinates: coordinates
        }
      }.to_json
    end

    def calculate_distance(lon1, lat1, lon2, lat2)
      dlon = (lon2 - lon1) * Math::PI / 180
      dlat = (lat2 - lat1) * Math::PI / 180
      mean_lat = (lat1 + lat2) / 2

      6371000 * Math.sqrt(dlat**2 + (Math.cos(mean_lat) * dlon)**2)
    end
    
    def get_city_name(lon, lat)
      uri = URI("https://nominatim.openstreetmap.org/reverse?lat=#{lat}&lon=#{lon}&format=json&zoom=10")
      req = Net::HTTP.get(uri)
      data = JSON.parse(req, {symbolize_names: true})

      if data[:address].has_key?(:city)
        data[:address][:city]
      elsif data[:address].has_key?(:town)
        data[:address][:town]
      elsif data[:address].has_key?(:village)
        data[:address][:village]
      else
        'Unknow location'
      end
    end

    def mkdir_p(path)
      Pathname.new(path).descend do |p|
        unless File.directory?(p) then Dir.mkdir(p) end
      end
    end

    def get_center(bbox)
      [ bbox[1] + (bbox[4] - bbox[1]) / 2,
        bbox[0] + (bbox[3] - bbox[0]) / 2 ]
    end

    private :convert, :calculate_distance, :get_city_name, :mkdir_p, :get_center
  end
end

module Jekyll
  module JourneyFilter
    def try_center(bbox)
      [ bbox[1] + (bbox[4] - bbox[1]) / 2,
        bbox[0] + (bbox[3] - bbox[0]) / 2 ]
    end

    def try_zoom(bbox)
      zlat = Math.log2(2.5*180 / (bbox[3] - bbox[0])) + 1
      zlon = Math.log2(360 / (bbox[4] - bbox[1])) + 1

      [zlat, zlon].min.floor
    end
  end
end

Liquid::Template.register_filter(Jekyll::JourneyFilter)
